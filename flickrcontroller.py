import urllib.request, json

SEARCH_URL = 'https://api.flickr.com/services/rest/?method=flickr.photos.search'

API_PARAM = '&api_key='
LAT_PARAM = '&lat='
LON_PARAM = '&lon='
PER_PAGE_PARAM = '&per_page='
PAGE_PARAM = '&page='
FORMAT_PARAM = '&format='

API_KEY = 'b8b5ee365c39800a925944f298b44e92'
PER_PAGE = '5'
PAGE = '1'
FORMAT = 'json'


# This method is responsible to retrieve 5 photo ids
# from flickr API and return it as an array of string.

# latitude: the latitude of the user (String)
# longitude: the longitude of the user (String)

# returns: an array of string containing 5 ids of photos
#          nearby the given lat and lon.

def getPhotoId(latitude, longitude):
    # Build the url.
    url = SEARCH_URL + API_PARAM + API_KEY + LAT_PARAM + latitude + LON_PARAM + longitude + PER_PAGE_PARAM + PER_PAGE + PAGE_PARAM + PAGE + FORMAT_PARAM + FORMAT

    # Send a GET request to the url, and retrieve the JSON.
    # It is advised to print this variable in order to understand it better.
    retrieved = urllib.request.urlopen(url).read()

    # Strip the outer part of json, because it has a header that wraps the json.
    retrieved = retrieved[14:-1] # [14:-1] means that we perform a substring to the retrieved json
                                 # from index 14 (inclusive) to the last index (exclusive).
                                 # Read python substring docs for more info.


    # The actual retrieved json was just a string; therefore, it needs to be parsed into a real json.
    data = json.loads(retrieved)

    # Get the values of 'photos' from the json.
    # It is advised to print this variable in order to understand it better
    page = data['photos']

    # Get the array of photo from 'photos'.
    # It is advised to print this variable in order to understand it better
    photos = page['photo']

    # This for loop will get the id of each photo in the array,
    # and put it inside a new array named photo_id
    photo_id = []
    for photo in photos:
        photo_id.append(photo['id'])

    return photo_id
